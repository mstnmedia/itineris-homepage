// In src/pages/Homepage.js

import React from 'react';
import NotFound from '../NotFound';
// import PrismicReact from 'prismic-reactjs';
import Prismic from 'prismic-javascript';
import Carousel from '../components/Carousel';
import Section from '../components/Section';


// Declare your component
export default class Homepage extends React.Component {

    state = {
        doc: null,
        notFound: false,
        carousel: [],
        categories: [],
        sections: []
    }

    categoriesGet(props) {
        if (!props.prismicCtx) return;
        var queryOptions = {
            page: '1',
            //orderings: '[my.post.date desc]'
        };

        // Query the posts
        const $this = this;
        props.prismicCtx.api.query(
            Prismic.Predicates.at("document.type", "categories"),
            queryOptions
        ).then(function (response) {
            $this.setState({ categories: response.results });
        });
    }

    componentWillMount() {
        this.fetchPage(this.props);
        this.categoriesGet(this.props);
    }

    componentWillReceiveProps(props) {
        this.fetchPage(props);
        this.categoriesGet(props);
    }

    componentDidUpdate() {
        this.props.prismicCtx.toolbar();
    }

    fetchPage(props) {
        if (props.prismicCtx) {
            var queryOptions = {
                page: '1',
                //orderings: '[my.post.date desc]'
            };
            return props.prismicCtx.api.query(
                Prismic.Predicates.at("document.type", "homepage"),
                queryOptions
            ).then((doc) => {
                const { carousel, sections } = doc.results[0].data;
                this.setState({ sections });
                // const promises = [];
                // for (var i = 0; i < carousel.length; i++) {
                //     promises.push(props.prismicCtx.api.getByID(carousel[i].carousel.id));
                //     //console.log(i);
                // }
                // Promise.all(promises).then(images => {
                //     //console.log("images array", images);
                //     this.setState({ carousel: images });
                // })
                //console.log(doc.results[0].data.carousel);

                const predicates = [];
                for (var i = 0; i < carousel.length; i++) {
                    predicates.push(Prismic.Predicates.at('document.id', carousel[i].carousel.id));
                }
                props.prismicCtx.api.query(predicates).then(images => {
                    const carouselImages = images.results[0].data.group;             
                    this.setState({ carousel: carouselImages});
                });
                this.setState({ doc });
            }).catch(err => {
                console.log(err);
                // this.setState({ notFound: !doc });
            });
        }
        return null;
    }

    categoriesRender() {
        return this.state.categories.map(category => <div>{category.data.name[0].text}</div>)
    }

    render() {
        if (this.state.doc) {
            return (
                <div data-wio-id={this.state.doc.id}>
                    <Carousel images={this.state.carousel} />
                    {this.state.sections.map((section,i)=><Section key={i} item={section} prismicCtx={this.props.prismicCtx}/>)}
                    {/* {this.categoriesRender.bind(this)()}

                    <img alt="cover" src={this.state.doc.data.image.url} />

                    <h1>{PrismicReact.RichText.asText(this.state.doc.data.title)}</h1>
                    {PrismicReact.RichText.render(this.state.doc.data.description, this.props.prismicCtx.linkResolver)} */}
                </div>
            );
        } else if (this.state.notFound) {
            return <NotFound />;
        }
        return <h1>Loading</h1>;
    }
}