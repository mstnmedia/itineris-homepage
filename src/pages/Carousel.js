// In src/Page.js

import React from 'react';
import NotFound from '../NotFound';
import PrismicReact from 'prismic-reactjs';
import Prismic from 'prismic-javascript';
import { Carousel } from 'react-bootstrap';



// Declare your component
export default class images extends React.Component {

    state = {
        doc: null,
        notFound: false,
        images: []
    }

    carouselGet(props) {
        if (!props.prismicCtx) return;
        var queryOptions = {
            page: '1',
        };

        const $this = this;
        props.prismicCtx.api.query(
            Prismic.Predicates.at("document.type", "carousel"),
            queryOptions
        ).then(function (response) {
            console.log(response);
            $this.setState({ images: response.results });
        });
    }

    componentWillMount() {
        this.fetchPage(this.props);
        this.carouselGet(this.props);
    }

    componentWillReceiveProps(props) {
        this.fetchPage(props);
        this.carouselGet(props);
    }

    componentDidUpdate() {
        this.props.prismicCtx.toolbar();
    }

    fetchPage(props) {
        if (props.prismicCtx) {
            // We are using the function to get a document by its uid
            return props.prismicCtx.api.getByUID('carousel', props.match.params.uid, {}, (err, doc) => {
                console.log(doc);
                if (doc) {
                    // We put the retrieved content in the state as a doc variable
                    this.setState({ doc });
                } else {
                    // We changed the state to display error not found if no matched doc
                    this.setState({ notFound: !doc });
                }
            });
        }
        return null;
    }

    CarouselRender() {
        return this.state.images.map(image =>

            <Carousel.Item>
                <img width={900} height={500} alt="9000x500" src={image.data.group[0].image.url} />
                <Carousel.Caption>
                    <h3>Itineris</h3>
                    <p>Itineris.do a new way to do things</p>
                </Carousel.Caption>
            </Carousel.Item>

        )
    }

    render() {
        if (this.state.doc) {
            return (
                <div data-wio-id={this.state.doc.id}>
                    <Carousel>
                        {this.CarouselRender.bind(this)()}
                    </Carousel>

                </div>
            );
        } else if (this.state.notFound) {
            return <NotFound />;
        }
        return <h1>Loading</h1>;
    }
}