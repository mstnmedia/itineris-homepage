import React from 'react';
import NotFound from '../NotFound';
import PrismicReact from 'prismic-reactjs';

export default class ChildrenDetail extends React.Component {
    ChildrenRender() {
        console.log('props item: ', this.props.item)
    }

    render() {
        console.log(this.props.item);
        console.log(this.props.state);
        return (
            <div>
                {this.ChildrenRender.bind(this)()}
            </div>
        );
    }
}