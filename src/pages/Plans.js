// In src/pages/Homepage.js

import React from 'react';
import NotFound from '../NotFound';
// import PrismicReact from 'prismic-reactjs';
import Prismic from 'prismic-javascript';
import Section from '../components/Section';
import Children from '../components/Childrens';
import Carousel from '../components/Carousel';

// Declare your component
export default class Plans extends React.Component {

    state = {
        doc: null,
        notFound: false,
        sections: [],
        carousel: [],
        childrens: [],
    }

    categoriesGet(props) {
        if (!props.prismicCtx) return;
        var queryOptions = {
            // page: '1',
            //orderings: '[my.post.date desc]'
            'fetchLinks': ['plan.name', 'service.name', 'industry.name', 'cube.name', 'categories.name']
        };

        // Query the posts
        // const $this = this;
        // props.prismicCtx.api.query(
        //     Prismic.Predicates.at("document.type", "plans"),
        //     queryOptions
        // )
        let query;
        const queryType = `my.${props.match.params.type}.uid`;
        console.log('queryType: ', props);
        if (props.match.params.uid) {
           query= Prismic.Predicates.at(queryType, props.match.params.uid);
        } else {
            query = Prismic.Predicates.at('document.type', props.match.params.type);
        }
        props.prismicCtx.api.query( (query), queryOptions)
        .then((doc) => {
            console.log('doc: ', doc.results[0].data);
            console.log('queryOption: ', queryOptions);
            const { carousel, sections, childrens } = doc.results[0].data;
            this.setState({ sections, childrens });
            const predicates = [];
            for (var i = 0; i < carousel.length; i++) {
                predicates.push(Prismic.Predicates.at('document.id', carousel[i].carousel.id));
            }
            props.prismicCtx.api.query(predicates).then(images => {
                const carouselImages = images.results[0].data.group;             
                this.setState({ carousel: carouselImages});
            });
            this.setState({ doc });
        }).catch(err => {
            console.log(err);
        });
    }

    componentWillMount() {
        this.categoriesGet(this.props);
    }

    componentWillReceiveProps(props) {
        this.categoriesGet(props);
    }

    componentDidUpdate() {
        this.props.prismicCtx.toolbar();
    }

    render() {
        console.log('state: ', this.state);
        if (this.state.doc) {
            return (
                <div data-wio-id={this.state.doc.id}>
                    {
                        this.state.carousel.length === 1 ?
                            <img alt="" src={this.state.carousel} /> :
                            <Carousel images={this.state.carousel} />
                    }
                    {this.state.sections.map(section => <Section item={section} prismicCtx={this.props.prismicCtx} />)}                    
                    {this.state.childrens.map(children => <Children item={children} prismicCtx={this.props.prismicCtx} />)}                    
                    
                    </div>
            );
        } else if (this.state.notFound) {
            return <NotFound />;
        }
        return <h1>Loading</h1>;
    }
}