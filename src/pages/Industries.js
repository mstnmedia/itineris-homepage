// In src/pages/Homepage.js

import React from 'react';
import NotFound from '../NotFound';
import PrismicReact from 'prismic-reactjs';
import Prismic from 'prismic-javascript';
import Section from '../components/Section';
import Carousel from '../components/Carousel';

// Declare your component
export default class Industries extends React.Component {

    state = {
        doc: null,
        notFound: false,
        sections: [],
        carousel: []
    }

    categoriesGet(props) {
        if (!props.prismicCtx) return;
        var queryOptions = {
            page: '1',
            //orderings: '[my.post.date desc]'
        };

        // Query the posts
        const $this = this;
        props.prismicCtx.api.query(
            Prismic.Predicates.at("document.type", "industries"),
            queryOptions
        ).then((doc) => {
            const { carousel, sections } = doc.results[0].data;
            this.setState({ sections });
            const promises = [];
            for (var i = 0; i < carousel.length; i++) {
                promises.push(props.prismicCtx.api.getByID(carousel[i].carousel.id));
                //console.log(i);
            }
            Promise.all(promises).then(images => {
                console.log("images array", images);
                this.setState({ carousel: images });
            })
            //console.log(doc.results[0].data.carousel);
            this.setState({ doc });
            console.log(this.state);
        }).catch(err => {
            console.log(err);
        });
    }

    componentWillMount() {
        this.categoriesGet(this.props);
    }

    componentWillReceiveProps(props) {
        this.categoriesGet(props);
    }

    componentDidUpdate() {
        this.props.prismicCtx.toolbar();
        console.log('carousel', this.state.carousel);
    }

    render() {
        if (this.state.doc) {
            return (
                <div data-wio-id={this.state.doc.id}>
                    {
                        this.state.carousel.length == 1 ?
                            <img src={this.state.carousel[0].data.group[0].image.url} /> :
                            <Carousel images={this.state.carousel} />
                    }
                    {this.state.sections.map(section => <Section item={section} prismicCtx={this.props.prismicCtx} />)}
                    {/* {this.categoriesRender.bind(this)()}

                    <img alt="cover" src={this.state.doc.data.image.url} />

                    <h1>{PrismicReact.RichText.asText(this.state.doc.data.title)}</h1>
                    {PrismicReact.RichText.render(this.state.doc.data.description, this.props.prismicCtx.linkResolver)} */}
                </div>
            );
        } else if (this.state.notFound) {
            return <NotFound />;
        }
        return <h1>Loading</h1>;
    }
}