import { combineReducers } from 'redux';
import AuthReducer from './AuthReducer';
import OrdersReducer from './OrdersReducer';
import ConfigReducer from './ConfigReducer';
import OffersReducer from './OffersReducer';
import ClientReducer from './ClientReducer';

export default combineReducers({
  auth: AuthReducer,
  orders: OrdersReducer,
  config: ConfigReducer,
  offers: OffersReducer,
  client: ClientReducer,
});

export const mapStateToProps = (state) => {
  const { config, auth, client, orders } = state;
  return { auth, config, client, orders };
};
