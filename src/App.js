import React from 'react';
import {
  BrowserRouter as Router,
  Route,
  Switch,
} from 'react-router-dom';
// import Preview from './Preview';
// import Help from './Help';
import NotFound from './NotFound';
// import Page from './Page';
import Homepage from './pages/Homepage';

// import Categories from './pages/Categories';
// import Carousel from './pages/Carousel';
// import Cubes from './pages/Cubes';
// import Industries from './pages/Industries';
import Plans from './pages/Plans';
// import CategoriesDetail from './pages/CategoriesDetail';
// import Services from './pages/Services';
// import ChildrenDetail from './pages/ChildrenDetail';
import Header from './components/Header';
import './App.css';

const App = (props) => (
  <div>
    <Header />
   
  <Router>
    <Switch>
    <Route exact path="/" render={routeProps => <Homepage {...routeProps} prismicCtx={props.prismicCtx} />} />      
    <Route exact path="/:type" render={routeProps => <Plans {...routeProps} prismicCtx={props.prismicCtx} />} />
      <Route exact path="/:type/:uid" render={routeProps => <Plans {...routeProps} prismicCtx={props.prismicCtx} />} />
      <Route component={NotFound} />
    </Switch>
  </Router>
  </div>
);

export default App;
