import React from 'react';
// import NotFound from '../NotFound';
import PrismicReact from 'prismic-reactjs';
// import Prismic from 'prismic-javascript';


export default class Section extends React.Component {
    render() {
        const { section_body } = this.props.item;
        return (
            <div>
               {PrismicReact.RichText.render(section_body, this.props.prismicCtx.linkResolver)} 
            </div>
        );
    }
}