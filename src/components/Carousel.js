// In src/Page.js

import React from 'react';
// import NotFound from '../NotFound';
// import PrismicReact from 'prismic-reactjs';
// import Prismic from 'prismic-javascript';
import { Carousel } from 'react-bootstrap';



// Declare your component
export default class images extends React.Component {

    CarouselRender() { 
        return this.props.images.map((image,i) =>
            <Carousel.Item key={i}>
                <img width={900} height={500} alt="9000x500" src={image.image.url} />
                <Carousel.Caption>
                    <h3>Itineris</h3>
                    <p>Itineris.do a new way to do things</p>
                </Carousel.Caption>
            </Carousel.Item>
        )
    }

    render() {
        if (this.props.images) {
            return (
                <div>
                    <Carousel>
                        {this.CarouselRender.bind(this)()}
                    </Carousel>

                </div>
            );
        } else {
            return <h1>Loading</h1>;
        }
        
    }
}