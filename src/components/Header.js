import React, { Component } from 'react';
import { } from 'react-bootstrap';
// import NavbarHeader from 'react-bootstrap/lib/NavbarHeader';
import NavItem from 'react-bootstrap/lib/NavItem';
import Nav from 'react-bootstrap/lib/Nav';
import Navbar from 'react-bootstrap/lib/Navbar';
// import NavbarCollapse from 'react-bootstrap/lib/NavbarCollapse';
// import NavbarBrand from 'react-bootstrap/lib/NavbarBrand';
// import NavbarToggle from 'react-bootstrap/lib/NavbarToggle';
import NavDropdown from 'react-bootstrap/lib/NavDropdown';
import MenuItem from 'react-bootstrap/lib/MenuItem';


class App extends Component {
  state = {
    isTop: true,
  };

  componentDidMount() {
    document.addEventListener('scroll', () => {
      const isTop = window.scrollY < 50;
      if (isTop !== this.state.isTop) {
        this.setState({ isTop })
      }
    });
  }

  render() {
    return (
      <Navbar inverse collapseOnSelect fixedTop className={this.state.isTop ? 'navbar-transparent' : 'navbar-scrolled'}>
        <Navbar.Header>
          <Navbar.Brand>
            <a href="/">Itineris</a>
          </Navbar.Brand>
          <Navbar.Toggle />
        </Navbar.Header>
        <Navbar.Collapse>
          <Nav className={this.state.isTop ? 'navbar-transparent' : ''}>
            <NavDropdown eventKey={1} title="Productos" id="basic-nav-dropdown">
              <MenuItem eventKey={1.1} href="Categories/erp">ERP</MenuItem>
              <MenuItem eventKey={1.2} href="Categories/sports">Sports</MenuItem>
              <MenuItem eventKey={1.3}>Something else here</MenuItem>
              <MenuItem divider />
              <MenuItem eventKey={1.4}>Separated link</MenuItem>
            </NavDropdown>
            <NavDropdown eventKey={1} title="Categorias" id="basic-nav-dropdown">
              <MenuItem eventKey={1.1}>Action</MenuItem>
              <MenuItem eventKey={1.2}>Another action</MenuItem>
              <MenuItem eventKey={1.3}>Something else here</MenuItem>
              <MenuItem divider />
              <MenuItem eventKey={1.4}>Separated link</MenuItem>
            </NavDropdown>
            <NavDropdown eventKey={2} title="Soluciones" id="basic-nav-dropdown">
              <MenuItem eventKey={2.1}>Action</MenuItem>
              <MenuItem eventKey={2.2}>Another action</MenuItem>
              <MenuItem eventKey={2.3}>Something else here</MenuItem>
              <MenuItem divider />
              <MenuItem eventKey={2.4}>Separated link</MenuItem>
            </NavDropdown>
            <NavDropdown eventKey={3} title="Servicios" id="basic-nav-dropdown">
              <MenuItem eventKey={3.1}>Action</MenuItem>
              <MenuItem eventKey={3.2}>Another action</MenuItem>
              <MenuItem eventKey={3.3}>Something else here</MenuItem>
              <MenuItem divider />
              <MenuItem eventKey={3.3}>Separated link</MenuItem>
            </NavDropdown>
            <NavItem eventKey={4} href="/plans">
              Planes
            </NavItem>
            <NavItem eventKey={5} href="#">
              Soporte
            </NavItem>
            <NavItem eventKey={6} href="#">
              Developers
            </NavItem>
          </Nav>
          <Nav pullRight>
            <NavItem eventKey={1} href="#">
              ES
            </NavItem>
            <NavItem eventKey={2} href="#">
              Login
            </NavItem>
            <NavItem eventKey={3} href="#">
             Sign In
            </NavItem>
            <NavItem eventKey={4} href="#">
             Try for free
            </NavItem>
            <NavItem eventKey={5} href="#">
            </NavItem>
          </Nav>
        </Navbar.Collapse>
      </Navbar>
    );
  }
}

export default App;
