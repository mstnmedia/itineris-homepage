import React from 'react';
// import NotFound from '../NotFound';
import PrismicReact from 'prismic-reactjs';
// import Prismic from 'prismic-javascript';
import { Link } from 'react-router-dom';


export default class Section extends React.Component {
    render() {
        // console.log('item: ', this.props.item.childrens.data.name[0]);
        const { childrens } = this.props.item;
        console.log('childrens: ', childrens);
        const childrenText = childrens.data.name;
        console.log('childrenText', childrenText);
        const url = `${childrens.type}/${childrens.uid}`;
        return (
            <div>
                <Link to={url}>
                    {PrismicReact.RichText.render(childrenText, this.props.prismicCtx.linkResolver)}
                </Link>
            </div>
        );
    }
}