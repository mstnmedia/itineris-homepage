import { GET_CATEGORIES } from './types';

import PrismicReact from 'prismic-reactjs';

export const CategoriesGet = () => ({
    type: 'GET_CATEGORIES'

})